 _dir = File.dirname(__FILE__)
class SubcommunityPluginController < MyProfileController
  def new_community
    @user = user
    unless profile.members.include? user
      throw 'You do not have permission to create subcommunity'
    end
    @community = Community.new params[:community]
    @community.environment = environment
    @community.theme = profile.theme
    if request.post? && @community.valid?
      @community.save!
      @community.add_admin user
      SubOrganizationsPlugin::Relation.add_children(profile, @community)
      redirect_to  @community.url
    end
  end
end
