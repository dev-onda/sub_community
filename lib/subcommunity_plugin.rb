class SubcommunityPlugin < Noosfero::Plugin

  def self.plugin_name
    "SubcommunityPlugin"
  end

  def self.plugin_description
    _("A plugin for creating Subcommunity")
  end

end
