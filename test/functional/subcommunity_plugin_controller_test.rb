require 'test_helper'

class SubcommunityPluginControllerTest < ActionController::TestCase

  def setup
    @controller = SubcommunityPluginController.new
    @profile = create_user('testuser').person
    login_as('testuser')
    @someone = create_user('someone').person
    @test = Community.create name:'test'
    @test.theme = 'my-theme'
    @test.add_member @profile
    @test.save!
  end

  attr_reader :profile, :test, :someone

  should 'the community have the theme' do
    assert_difference 'Community.count' do
      post :new_community, profile:profile.identifier, :community => {name:'My test', description:'Community for test'}
      assert_response :redirect
      assert Community['my-test']
      assert_equal 'my-theme', Community['my-test'].theme
    end
  end

  should 'user is not a member' do
    assert_no_difference 'Community.count' do
      login_as('someone')
      assert_raise ArgumentError do
        post :new_community, profile:someone.identifier, :community => {name:'My test', description:'Community for test'}
      end
      assert_equal Community['teste'], nil
    end
  end

  should 'the subcommunity is daughter of the higher community' do
    assert_difference 'Community.count' do
      post :new_community, profile:profile.identifier, :community => {name:'My test', description:'Community for test'}
      assert_response :redirect
      assert Community['my-test']
      children = Community.children(Community['test'])
      assert_equal children.include?(Community['my-test']), true
    end
  end

end
